package com.uk.android.core.api.model

import com.google.gson.annotations.SerializedName
import com.uk.android.ui.characters.domain.CharacterLocation
import com.uk.android.ui.characters.domain.CharacterOrigin
import com.uk.android.ui.characters.domain.Character

class CharactersResponse(
        @SerializedName("info") val info: ResponseInfo,
        @SerializedName("results") val results: List<CharacterRemote>
) {
    companion object
}

class CharacterRemote(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("status") val status: String,
        @SerializedName("species") val species: String,
        @SerializedName("type") val type: String,
        @SerializedName("gender") val gender: String,
        @SerializedName("origin") val origin: CharacterOriginRemote,
        @SerializedName("location") val location: CharacterLocationRemote,
        @SerializedName("image") val image: String,
        @SerializedName("episode") val episodes: List<String>,
        @SerializedName("url") val url: String,
        @SerializedName("created") val created: String
) {
    fun toCharacter() = Character(
        id,
        name,
        status,
        species,
        type,
        gender,
        origin.toCharacterOrigin(),
        location.toCharacterLocation(),
        image,
        episodes,
        url
    )

    companion object
}


class CharacterOriginRemote(
        @SerializedName("name") val name: String,
        @SerializedName("url") val url: String
) {

    fun toCharacterOrigin() = CharacterOrigin(name, url)
}

class CharacterLocationRemote(
        @SerializedName("name") val name: String,
        @SerializedName("url") val url: String
) {

    fun toCharacterLocation() = CharacterLocation(name, url)
}