package com.uk.android.core.network

interface NetworkStateProvider {
    fun isNetworkAvailable(): Boolean
}