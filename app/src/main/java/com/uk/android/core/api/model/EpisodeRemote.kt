package com.uk.android.core.api.model

import com.google.gson.annotations.SerializedName
import com.uk.android.ui.episodes.domain.Episode

data class EpisodeRemote(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("air_date") val airDate: String,
        @SerializedName("episode") val code: String,
        @SerializedName("characters") val characters: List<String>,
        @SerializedName("url") val url: String,
        @SerializedName("created") val created: String
) {
    fun toEpisode() = Episode(
        id,
        name,
        airDate,
        code,
        characters,
        url
    )

    companion object

}