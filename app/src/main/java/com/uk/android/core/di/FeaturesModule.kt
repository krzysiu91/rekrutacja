package com.uk.android.core.di

import com.uk.android.ui.characters.all.CharacterAdapter
import com.uk.android.ui.characters.all.CharactersFragment
import com.uk.android.ui.characters.all.CharactersViewModel
import com.uk.android.ui.characters.data.remote.CharacterRepositoryImpl
import com.uk.android.ui.characters.details.CharacterDetailsFragment
import com.uk.android.ui.characters.details.CharacterDetailsViewModel
import com.uk.android.ui.characters.domain.CharacterRepository
import com.uk.android.ui.characters.domain.GetCharactersUseCase
import com.uk.android.ui.characters.navigator.CharacterNavigator
import com.uk.android.ui.characters.navigator.CharacterNavigatorImpl
import com.uk.android.ui.episodes.data.repository.EpisodeRepositoryImpl
import com.uk.android.ui.episodes.domain.EpisodeRepository
import com.uk.android.ui.episodes.domain.GetEpisodesUseCase
import com.uk.android.ui.episodes.presentation.EpisodeAdapter
import com.uk.android.ui.episodes.presentation.EpisodesFragment
import com.uk.android.ui.episodes.presentation.EpisodesViewModel
import com.uk.android.ui.location.LocationsFragment
import com.uk.android.ui.location.LocationsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module


val featuresModule = module {
    factory<EpisodeRepository> { EpisodeRepositoryImpl(get(), get(), get(), get()) }
    factory<CharacterRepository> { CharacterRepositoryImpl(get(), get(), get(), get()) }

    factory { GetEpisodesUseCase(get()) }
    factory { GetCharactersUseCase(get()) }

    factory<CharacterNavigator> { CharacterNavigatorImpl(get()) }

    scope(named<CharactersFragment>()) {
        viewModel { CharactersViewModel(get(), get(), get()) }
        factory { CharacterAdapter() }
    }

    scope(named<EpisodesFragment>()) {
        viewModel { EpisodesViewModel(get(), get()) }
        factory { EpisodeAdapter() }
    }

    scope(named<LocationsFragment>()) {
        viewModel { LocationsViewModel() }
    }

    scope(named<CharacterDetailsFragment>()) {
        viewModel { CharacterDetailsViewModel() }
    }

}