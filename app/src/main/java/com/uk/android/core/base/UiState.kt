package com.uk.android.core.base

sealed class UiState {
    object Idle : UiState()
    object Pending : UiState()
}