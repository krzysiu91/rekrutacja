package com.uk.android.core.exception

interface ErrorMapper {
    fun map(throwable: Throwable): String
}