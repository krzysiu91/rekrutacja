package com.uk.android.core.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.uk.android.ui.characters.data.local.CharacterCached
import com.uk.android.ui.characters.data.local.CharacterDao
import com.uk.android.ui.episodes.data.local.EpisodeDao
import com.uk.android.ui.episodes.data.local.model.EpisodeCached

@Database(
    entities = [EpisodeCached::class, CharacterCached::class],
    version = 1
)
@TypeConverters(ListConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun episodeDao(): EpisodeDao
    abstract fun characterDao(): CharacterDao
}