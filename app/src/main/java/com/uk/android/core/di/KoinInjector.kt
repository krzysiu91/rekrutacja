package com.uk.android.core.di

val koinInjector = featuresModule +
        appModule +
        networkModule +
        databaseModule