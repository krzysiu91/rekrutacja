package com.uk.android.core.di

import android.content.Context
import android.net.ConnectivityManager
import androidx.navigation.navOptions
import androidx.recyclerview.widget.DividerItemDecoration
import com.uk.android.R
import com.uk.android.core.exception.ErrorMapper
import com.uk.android.core.exception.ErrorMapperImpl
import com.uk.android.core.exception.ErrorWrapper
import com.uk.android.core.exception.ErrorWrapperImpl
import com.uk.android.core.navigation.FragmentNavigator
import com.uk.android.core.navigation.FragmentNavigatorImpl
import com.uk.android.core.network.NetworkStateProvider
import com.uk.android.core.network.NetworkStateProviderImpl
import com.uk.android.core.provider.ActivityProvider
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val appModule = module {
    factory { DividerItemDecoration(androidContext(), DividerItemDecoration.VERTICAL) }
    factory { androidContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }
    factory<NetworkStateProvider> { NetworkStateProviderImpl(get()) }
    factory<ErrorWrapper> { ErrorWrapperImpl() }
    factory<ErrorMapper> { ErrorMapperImpl(androidContext()) }
    single(createdAtStart = true) { ActivityProvider(androidApplication()) }
    factory<FragmentNavigator> {
        FragmentNavigatorImpl(
            activityProvider = get(),
            navHostFragmentRes = R.id.nav_host_fragment_activity_main,
            homeDestinationRes = R.id.navigation_episodes,
        )
    }
}