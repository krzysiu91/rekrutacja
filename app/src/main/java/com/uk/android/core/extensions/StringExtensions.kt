package com.uk.android.core.extensions

fun String.Companion.empty() = ""

fun String.getOrNullIfUnknown() =
    if (this == "unknown") null
    else this