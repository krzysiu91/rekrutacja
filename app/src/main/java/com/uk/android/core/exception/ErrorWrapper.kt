package com.uk.android.core.exception

interface ErrorWrapper {
    fun wrap(throwable: Throwable): Throwable
}