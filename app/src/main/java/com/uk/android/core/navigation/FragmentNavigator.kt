package com.uk.android.core.navigation

import androidx.annotation.IdRes

interface FragmentNavigator {

    fun navigateTo(
            @IdRes destinationId: Int,
    )

    fun <T> navigateTo(
            @IdRes destinationId: Int,
            param: Pair<String, T>? = null,
    )

    fun goBack(
            @IdRes destinationId: Int? = null,
            inclusive: Boolean = false
    )

    fun clearHistory()
}