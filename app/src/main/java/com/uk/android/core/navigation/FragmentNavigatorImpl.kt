package com.uk.android.core.navigation

import androidx.annotation.IdRes
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.uk.android.core.provider.ActivityProvider

class FragmentNavigatorImpl(
        private val activityProvider: ActivityProvider,
        @IdRes private val navHostFragmentRes: Int,
        @IdRes private val homeDestinationRes: Int,
) : FragmentNavigator {

    private fun getSupportFragmentManager() =
        (activityProvider.foregroundActivity as? FragmentActivity)?.supportFragmentManager

    private fun getNavController() = getSupportFragmentManager()
            ?.findFragmentById(navHostFragmentRes)
            ?.findNavController()

    override fun navigateTo(destinationId: Int) {
        navigateTo<Unit>(destinationId, null)
    }

    override fun <T> navigateTo(
            destinationId: Int,
            param: Pair<String, T>?,

    ) {
        val bundle = param?.let { bundleOf(it) }

        getNavController()?.navigate(destinationId, bundle)
    }

    override fun goBack(destinationId: Int?, inclusive: Boolean) {
        if (destinationId == null) getNavController()?.popBackStack()
        else getNavController()?.popBackStack(destinationId, inclusive)
    }

    override fun clearHistory() {
        goBack(homeDestinationRes)
    }
}