package com.uk.android.core.api

import com.uk.android.core.api.model.CharactersResponse
import com.uk.android.core.api.model.EpisodesResponse
import retrofit2.http.GET

interface ApiService {

    @GET("episode/")
    suspend fun getEpisodes(): EpisodesResponse

    @GET("character")
    suspend fun getCharacters(): CharactersResponse
}
