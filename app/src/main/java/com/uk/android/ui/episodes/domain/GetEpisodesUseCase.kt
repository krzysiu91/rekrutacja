package com.uk.android.ui.episodes.domain

import com.uk.android.core.base.UseCase

class GetEpisodesUseCase(private val repository: EpisodeRepository) :
    UseCase<List<Episode>, Unit>() {

    override suspend fun action(params: Unit) = repository.getEpisodes()

}