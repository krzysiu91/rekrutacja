package com.uk.android.ui.characters.domain

import com.uk.android.core.base.UseCase


class GetCharactersUseCase(private val repository: CharacterRepository) :
    UseCase<List<Character>, Unit>() {

    override suspend fun action(params: Unit) = repository.getCharacters()

}