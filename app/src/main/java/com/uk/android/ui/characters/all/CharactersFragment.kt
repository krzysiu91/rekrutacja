package com.uk.android.ui.characters.all

import com.uk.android.R
import com.uk.android.core.base.BaseFragment
import com.uk.android.core.extensions.viewBinding
import com.uk.android.databinding.FragmentCharactersBinding
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel

class CharactersFragment : BaseFragment<CharactersViewModel>(R.layout.fragment_characters) {

    private val binding by viewBinding(FragmentCharactersBinding::bind)
    private val characterAdapter: CharacterAdapter by lifecycleScope.inject()

    override val viewModel: CharactersViewModel by lifecycleScope.viewModel(this)

    override fun initViews() {
        super.initViews()
        initRecycler()
    }

    override fun initObservers() {
        super.initObservers()
        observeEpisodes()
    }

    private fun observeEpisodes() {
        viewModel.characters.observe(this) {
            characterAdapter.setCharacters(it)
        }
    }

    private fun initRecycler() {
        with(binding.recyclerView) {
            setHasFixedSize(true)
            adapter = characterAdapter
        }
        characterAdapter.setOnCharacterClickListener { viewModel.onCharacterClick(it) }
    }
}
