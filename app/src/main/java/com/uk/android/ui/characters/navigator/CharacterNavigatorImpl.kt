package com.uk.android.ui.characters.navigator

import com.uk.android.R
import com.uk.android.core.navigation.FragmentNavigator
import com.uk.android.ui.characters.all.CharacterDisplayable
import com.uk.android.ui.characters.details.CharacterDetailsFragment

class CharacterNavigatorImpl(
        private val fragmentNavigator: FragmentNavigator
) : CharacterNavigator {

    override fun openCharacterDetailsScreen(character: CharacterDisplayable) {
        fragmentNavigator.navigateTo(
            R.id.action_navigate_from_screen_characters_to_screen_character_details,
            CharacterDetailsFragment.CHARACTER_DETAILS_KEY to character
        )
    }

    override fun goBack() {
        fragmentNavigator.goBack()
    }
}