package com.uk.android.ui.episodes.presentation

import com.uk.android.ui.episodes.domain.Episode

data class EpisodeDisplayable(
        val name: String,
        val airDate: String,
        val code: String
) {

    constructor(episode: Episode) : this(
        episode.name,
        episode.airDate,
        episode.code
    )

    val fullName = "$code: $name"
}