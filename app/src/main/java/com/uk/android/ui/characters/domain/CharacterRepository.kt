package com.uk.android.ui.characters.domain



interface CharacterRepository {
    suspend fun getCharacters(): List<Character>
}