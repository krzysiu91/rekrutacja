package com.uk.android.ui.characters.data.remote

import com.uk.android.core.api.ApiService
import com.uk.android.core.exception.ErrorWrapper
import com.uk.android.core.exception.callOrThrow
import com.uk.android.core.network.NetworkStateProvider
import com.uk.android.ui.characters.data.local.CharacterCached
import com.uk.android.ui.characters.data.local.CharacterDao
import com.uk.android.ui.characters.domain.CharacterRepository
import com.uk.android.ui.characters.domain.Character

class CharacterRepositoryImpl(
        private val api: ApiService,
        private val dao: CharacterDao,
        private val networkStateProvider: NetworkStateProvider,
        private val errorWrapper: ErrorWrapper
) : CharacterRepository {

    override suspend fun getCharacters(): List<Character> {
        return if (networkStateProvider.isNetworkAvailable()) {
            callOrThrow(errorWrapper) { getCharactersFromRemote() }
                    .also { saveCharactersToLocal(it) }
        } else {
            getCharactersFromLocal()
        }
    }

    private suspend fun getCharactersFromRemote(): List<Character> {
        return api.getCharacters()
                .results
                .map { it.toCharacter() }
    }

    private suspend fun getCharactersFromLocal(): List<Character> {
        return dao.getCharacters()
                .map { it.toCharacter() }
    }

    private suspend fun saveCharactersToLocal(characters: List<Character>) {
        characters.map { CharacterCached(it) }
                .toTypedArray()
                .let { dao.saveCharacters(*it) }
    }
}