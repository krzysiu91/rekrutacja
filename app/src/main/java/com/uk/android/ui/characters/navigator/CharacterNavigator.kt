package com.uk.android.ui.characters.navigator

import com.uk.android.ui.characters.all.CharacterDisplayable

interface CharacterNavigator {
    fun openCharacterDetailsScreen(character: CharacterDisplayable)
    fun goBack()
}