package com.uk.android.ui.episodes.domain

interface EpisodeRepository {
    suspend fun getEpisodes(): List<Episode>
}