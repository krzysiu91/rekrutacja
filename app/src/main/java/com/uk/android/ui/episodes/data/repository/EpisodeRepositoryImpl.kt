package com.uk.android.ui.episodes.data.repository

import com.uk.android.core.api.ApiService
import com.uk.android.core.exception.ErrorWrapper
import com.uk.android.core.exception.callOrThrow
import com.uk.android.core.network.NetworkStateProvider
import com.uk.android.ui.episodes.data.local.EpisodeDao
import com.uk.android.ui.episodes.data.local.model.EpisodeCached
import com.uk.android.ui.episodes.domain.Episode
import com.uk.android.ui.episodes.domain.EpisodeRepository

class EpisodeRepositoryImpl(
        private val api: ApiService,
        private val dao: EpisodeDao,
        private val networkStateProvider: NetworkStateProvider,
        private val errorWrapper: ErrorWrapper
) : EpisodeRepository {

    override suspend fun getEpisodes(): List<Episode> {
        return if (networkStateProvider.isNetworkAvailable()) {
            callOrThrow(errorWrapper) { getEpisodesFromRemote() }
                    .also { saveEpisodesToLocal(it) }
        } else {
            getEpisodesFromLocal()
        }
    }

    private suspend fun getEpisodesFromRemote(): List<Episode> {
        return api.getEpisodes()
                .results
                .map { it.toEpisode() }
    }

    private suspend fun getEpisodesFromLocal(): List<Episode> {
        return dao.getEpisodes()
                .map { it.toEpisode() }
    }

    private suspend fun saveEpisodesToLocal(episodes: List<Episode>) {
        episodes.map { EpisodeCached(it) }
                .toTypedArray()
                .let { dao.saveEpisodes(*it) }

    }
}