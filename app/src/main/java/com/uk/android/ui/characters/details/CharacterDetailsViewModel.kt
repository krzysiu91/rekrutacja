package com.uk.android.ui.characters.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.uk.android.core.base.BaseViewModel
import com.uk.android.ui.characters.all.CharacterDisplayable

class CharacterDetailsViewModel : BaseViewModel() {

    private val _character by lazy { MutableLiveData<CharacterDisplayable>() }
    val character: LiveData<CharacterDisplayable> by lazy { _character }

    fun onCharacterPassed(character: CharacterDisplayable) {
        _character.value = character
    }

}