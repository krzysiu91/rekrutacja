package com.uk.android.ui.episodes.presentation

import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import com.uk.android.R
import com.uk.android.core.base.BaseFragment
import com.uk.android.core.extensions.viewBinding
import com.uk.android.databinding.FragmentEpisodesBinding
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel

class EpisodesFragment : BaseFragment<EpisodesViewModel>(R.layout.fragment_episodes) {

    private val binding by viewBinding(FragmentEpisodesBinding::bind)
    private val divider: DividerItemDecoration by lifecycleScope.inject()
    private val episodeAdapter: EpisodeAdapter by lifecycleScope.inject()

    override val viewModel: EpisodesViewModel by lifecycleScope.viewModel(this)

    override fun initViews() {
        super.initViews()
        initRecycler()
    }

    override fun initObservers() {
        super.initObservers()
        observeEpisodes()
    }

    override fun onIdleState() {
        super.onIdleState()

        binding.progressBar.visibility = View.GONE
    }

    override fun onPendingState() {
        super.onPendingState()

        binding.progressBar.visibility = View.VISIBLE
    }

    private fun observeEpisodes() {
        viewModel.episodes.observe(this) {
            episodeAdapter.setItems(it)
        }
    }

    private fun initRecycler() {
        with(binding.recyclerView) {
            addItemDecoration(divider)
            setHasFixedSize(true)
            adapter = episodeAdapter
        }
    }
}
