package com.uk.android.ui.characters.details

import android.os.Bundle
import androidx.lifecycle.observe
import com.bumptech.glide.Glide
import com.uk.android.R
import com.uk.android.core.base.BaseFragment
import com.uk.android.core.extensions.viewBinding
import com.uk.android.databinding.FragmentCharacterDetailsBinding
import com.uk.android.ui.characters.all.CharacterDisplayable
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel

class CharacterDetailsFragment :
    BaseFragment<CharacterDetailsViewModel>(R.layout.fragment_character_details) {

    private val binding by viewBinding(FragmentCharacterDetailsBinding::bind)
    override val viewModel: CharacterDetailsViewModel by lifecycleScope.viewModel(this)

    companion object {
        const val CHARACTER_DETAILS_KEY = "characterDetailsKey"
    }

    override fun initObservers() {
        super.initObservers()
        observeCharacter()
    }

    private fun observeCharacter() {
        viewModel.character.observe(this) { showCharacter(it) }
    }

    private fun showCharacter(character: CharacterDisplayable) {
        with(binding) {
            Glide.with(this@CharacterDetailsFragment)
                    .load(character.image)
                    .into(characterImage)

            val nameLabel = getString(R.string.character_name)
            characterName.text = String.format(nameLabel, character.name)

            val originLabel = getString(R.string.character_origin)
            characterOrigin.text = String.format(originLabel, character.originName)

            val locationLabel = getString(R.string.character_current_location)
            characterCurrentLocation.text = String.format(locationLabel, character.locationName)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notifyAboutData()
    }

    private fun notifyAboutData() {
        arguments
                ?.getParcelable<CharacterDisplayable>(CHARACTER_DETAILS_KEY)
                ?.let { viewModel.onCharacterPassed(it) }
    }
}