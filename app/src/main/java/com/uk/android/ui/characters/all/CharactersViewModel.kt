package com.uk.android.ui.characters.all

import androidx.lifecycle.*
import com.uk.android.core.base.BaseViewModel
import com.uk.android.core.exception.ErrorMapper
import com.uk.android.ui.characters.domain.GetCharactersUseCase
import com.uk.android.ui.characters.navigator.CharacterNavigator
import com.uk.android.ui.characters.domain.Character

class CharactersViewModel(
        private val getCharactersUseCase: GetCharactersUseCase,
        private val characterNavigator: CharacterNavigator,
        errorMapper: ErrorMapper
) : BaseViewModel(errorMapper) {

    private val _characters by lazy {
        MutableLiveData<List<Character>>()
                .also { getCharacters(it) }
    }

    val characters: LiveData<List<CharacterDisplayable>> by lazy {
        _characters.map { characters ->
            characters.map { CharacterDisplayable(it) }
        }
    }

    private fun getCharacters(liveData: MutableLiveData<List<Character>>) {
        setPendingState()
        getCharactersUseCase(Unit, viewModelScope) { result ->
            setIdleState()
            result.onSuccess { liveData.value = it }
            result.onFailure { handleFailure(it) }
        }
    }

    fun onCharacterClick(character: CharacterDisplayable) {
        characterNavigator.openCharacterDetailsScreen(character)
    }
}