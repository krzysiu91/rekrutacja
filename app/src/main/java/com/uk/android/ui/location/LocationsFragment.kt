package com.uk.android.ui.location

import com.uk.android.R
import com.uk.android.core.base.BaseFragment
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.androidx.viewmodel.scope.viewModel

class LocationsFragment : BaseFragment<LocationsViewModel>(R.layout.fragment_locations) {
    override val viewModel: LocationsViewModel by lifecycleScope.viewModel(this)
}