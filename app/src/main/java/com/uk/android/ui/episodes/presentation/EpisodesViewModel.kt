package com.uk.android.ui.episodes.presentation


import androidx.lifecycle.*
import com.uk.android.core.base.BaseViewModel
import com.uk.android.core.exception.ErrorMapper
import com.uk.android.ui.episodes.domain.Episode
import com.uk.android.ui.episodes.domain.GetEpisodesUseCase

class EpisodesViewModel(
        private val getEpisodesUseCase: GetEpisodesUseCase,
        errorMapper: ErrorMapper
) : BaseViewModel(errorMapper) {

    private val _episodes by lazy {
        MutableLiveData<List<Episode>>()
                .also { getEpisodes(it) }
    }

    val episodes: LiveData<List<EpisodeDisplayable>> by lazy {
        _episodes.map { episodes ->
            episodes.map { EpisodeDisplayable(it) }
        }
    }

    private fun getEpisodes(episodeLiveData: MutableLiveData<List<Episode>>) {
        setPendingState()
        getEpisodesUseCase(Unit, viewModelScope) { result ->
            setIdleState()
            result.onSuccess { episodeLiveData.value = it }
            result.onFailure { handleFailure(it) }
        }
    }


}